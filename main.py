from typing import Union
from pathlib import Path
import pandas as pd

from utils import read_txt, save_txt
from utils.preprocessing.preprocessing import preprocess_text
from utils.processing import slide_window, tokens_only


def preprocess_corpora(dataset_dir: Union[Path, str]) -> Path:
    preprocessing_dir = Path(dataset_dir).parent / "preprocessed"
    
    if not preprocessing_dir.exists():
        preprocessing_dir.mkdir(parents=True)
    
    for filepath in dataset_dir.glob("*.txt"):
        text = read_txt(filename=filepath)
        text = preprocess_text(text)
        
        save_txt(
            filename=preprocessing_dir / filepath.name, 
            text=text
        )
    
    return preprocessing_dir


def process_corpora(dataset_dir: Union[Path, str]) -> Path:
    processing_dir = Path(dataset_dir).parent / "processed"
    
    if not processing_dir.exists():
        processing_dir.mkdir(parents=True)
    
    for filepath in dataset_dir.glob("*.txt"):
        text = read_txt(filename=filepath)
        text = tokens_only(text)
        
        save_txt(
            filename=processing_dir / filepath.name, 
            text=text
        )         
        
    return processing_dir   


if __name__ == "__main__":
    dataset_dir = Path("dataset/raw")
    
    if not dataset_dir.exists():
        print("Dataset directory does not exist.")
    else:
        preprocessing_dir = preprocess_corpora(dataset_dir)
        processing_dir = process_corpora(preprocessing_dir)
        
        df = pd.DataFrame()
        
        for filename in processing_dir.glob("*.txt"):
            text = read_txt(filename=filename)
            
            windows_df = slide_window(text)            
            df = pd.concat([df, windows_df], copy=False)
        
        df.to_csv("corpora_without_tokens.csv", index=False)