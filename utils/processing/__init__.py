import re
from typing import List, Tuple

import pandas as pd


def tokens_only(text: str) -> str:
    table = {
        r"(и){(з)-}(а)": r"\1\2-\2\3",
        r"{(\w)-}": r"\1",
    }
    
    for pattern, repl in table.items():
        text = re.sub(
            pattern=pattern,
            repl=repl,
            string=text,
            flags=re.IGNORECASE
        )
        
    text = ' '.join(text.split())    
        
    return text


def tokenize(text: str) -> List[str]:
    """Tokenizes input string. For now simple str.split is enough
    Args:
        text (str): input string

    Returns:
        list[str]: list of tokens
    """
    
    return text.split()
    
    
def is_special_token(token: str) -> bool:
    return re.match(pattern=r"{.+?}", string=token) != None 


def _split_word_token(tokenized_text: List[str]) -> Tuple[List[str], List[str]]:
    words = []
    tokens = []
    
    for i in range(len(tokenized_text)):
        if is_special_token(tokenized_text[i]):
            tokens.append(tokenized_text[i])
        else:
            words.append(tokenized_text[i])
    
    return words, tokens
    
    
def slide_window(
    text: str, 
    window_size: int = 4,
    space_token: str = "{SPACE}",
    leave_target: bool = False
) -> pd.DataFrame:
    """Slides a window of set window size through the given text

    Args:
        text (str): input string
        window_size (int, optional): Defaults to 4.
        space_token (str, optional): whitespace token. Defaults to "{SPACE}".
        leave_target (bool, optional): if leave_target is True then target values will be in window.
            Otherwise they won't. Defaults to False.
            For example, leave_target is
                        * True: word_n-1 ... [<word_n>, {ANOTHER_TOKEN}, <word_n+1>, <word_n+2>] ... word_n+3 --> {TOKEN}
                        * False: word_n-1 ... [<word_n>, <word_n+1>, <word_n+2>, <word_n+3>] ... word_n+4 --> {TOKEN}

    Returns:
        pandas.DataFrame: DataFrame which contains data in two columns ('text', 'transcription')
    """
    
    columns = ["text", "transcription"]
    
    tokenized_text = tokenize(text)
    words, tokens = _split_word_token(tokenized_text=tokenized_text)
    half_ws = window_size // 2
    
    windows = set()
    
    words.insert(0, '')
    words.append('')
    
    token_idx = 0
    
    for word_idx in range(len(words) - half_ws):
        window = ' '.join(words[word_idx:word_idx + window_size])
        is_token_sequence = False
        
        while True:
            idx = (word_idx * 2 + window_size) // 2 + token_idx - 1
            
            if token_idx >= len(tokens):
                windows.add((window, space_token))
                break
            
            if tokenized_text[idx] != tokens[token_idx]:
                if not is_token_sequence: 
                    windows.add((window, space_token))
                break
            else:
                windows.add((window, tokens[token_idx]))
                is_token_sequence = True
                
            token_idx += 1
    
    return pd.DataFrame(data=windows, columns=columns)