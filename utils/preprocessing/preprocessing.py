from pathlib import Path
from typing import Callable, List
from unicodedata import normalize, category
import re

from utils import save_txt


def remove_line_if(text: str, condition: Callable[[str], bool]) -> str:
    """Remove lines for which condition function returns True
    
    Args:
        text (str): input string
        condition (Callable[[str], bool]): function with one argument (line: str)

    Returns:
        str: string without those lines
    """
    
    return '\n'.join(filter(lambda line: not condition(line), text.splitlines()))


def strip_accents(text: str, exceptions: List[str] = []) -> str:
    """Removes accent marks in the given input string,

    Args:
        text (str): input string
        exceptions (list[str]): exceptions which are not supposed to be stripped

    Returns:
        str: string without accent marks

    Examples:
    >>> strip_accents("А́а́, Е́е́, И́и́, О́о́, У́у́, Ы́ы́, Э́э́, Ю́ю́, Я́я́, Йй")
    'Аа, Ее, Ии, Оо, Уу, Ыы, Ээ, Юю, Яя, Ии'

    """
    
    new_text = []
    for ch in text:
        if exceptions is not None and ch in exceptions:
            new_text.append(ch)
        else:
            new_text.extend([nch for nch in normalize("NFD", ch) if category(nch) != "Mn"])
    
    return ''.join(new_text)


def translate_symbols(text: str, table: dict) -> str:
    """Wrapper for str.translate

    Args:
        text (str): input string
        table (dict): Translation table, which must be a mapping of Unicode ordinals to
            Unicode ordinals, strings, or None.

    Returns:
        str: translated string
    """
    trans_table = str.maketrans(table)
    return text.translate(trans_table)


def replace_symbols(text: str, table: dict) -> str:
    """Wrapper for str.replace

    Args:
        text (str): input string
        table (dict): Translation table, which must be a mapping of Unicode ordinals to
            Unicode ordinals, strings, or None.

    Returns:
        str: string with replaced characters
    """
    for pattern, repl in table.items():
        text = text.replace(pattern, repl)
        
    return text


def replace_symbols_re(text: str, table: dict) -> str:
    """Wrapper for re.sub

    Args:
        text (str): input string
        table (dict): Translation table, which must be a mapping of Unicode ordinals to
            Unicode ordinals, strings, or None.

    Returns:
        str: string with replaced characters
    """
    for pattern, repl in table.items():
        text = re.sub(
            pattern=pattern,
            repl=repl,
            string=text,
            flags=re.IGNORECASE
        )
    
    return text


def preprocess_text(text: str) -> str:
    """Preprocessing function for text
    Replaces characters, and removes accent marks, and so on

    Args:
        text (str): input string

    Returns:
        str: preprocessed string
    """
    
    from . import replacements as repl
    
    
    text = remove_line_if(text, lambda line: "eduext" in line)
    text = strip_accents(text, set("йЙёЁ"))
    
    text = replace_symbols_re(text, repl.REPLACE_SYMBOLS_RE)
    text = translate_symbols(text, repl.EN_RU_LETTERS)
    text = replace_symbols(text, repl.REPLACE_SYMBOLS)
    text = replace_symbols_re(text, repl.REPLACE_SYMBOLS_RE)
    text = translate_symbols(text, repl.TRANSLATE_SYMBOLS)
    
    text = re.sub(
        pattern=r"({\w+?})",
        repl=r" \1 ",
        string=text,
    )
    
    return text
    
    
if __name__ == "__main__":
    dataset_dir = Path(__file__).parent.parent.parent / "dataset"
    
    preprocessed_dir = dataset_dir / "preprocessed"
    
    if not preprocessed_dir.exists():
        preprocessed_dir.mkdir(parents=True)
    
    for filepath in (dataset_dir / "raw").glob("*.txt"):
        with open(filepath, "r") as fp:
            text = fp.read()
            
            text = preprocess_text(text)
            save_txt(
                filename=preprocessed_dir / filepath.name, 
                text=text
            )
            