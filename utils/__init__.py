from pathlib import Path
from typing import Union
from xml.sax import saxutils as su


def save_txt(filename: Union[Path, str], text: str, encoding: str = "utf-8") -> None:
    """Simple function to save txt files

    Args:
        filename (Union[Path, str]):
        text (str): input string
    """
    
    with open(filename, "w") as fp:
        text = su.unescape(text)
        fp.write(text)
        
        
def read_txt(filename: Union[Path, str], encoding: str = "utf-8") -> str:
    """Simple function to read txt files

    Args:
        filename (Union[Path, str]):
        encoding (str, optional): Defaults to "utf-8".

    Returns:
        str: txt file's content
    """
    
    with open(filename, encoding=encoding) as fp:
        text = fp.read()
        return text