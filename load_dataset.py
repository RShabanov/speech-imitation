from argparse import ArgumentParser
from concurrent.futures import ThreadPoolExecutor, as_completed
from enum import IntEnum, auto
import json
from pathlib import Path
from typing import Dict, List, Tuple, Union
import requests
from bs4 import BeautifulSoup

from utils import save_txt


class TRANSCRIPTION_TYPE(IntEnum):
    FULL = 1
    SIMPLE = auto()
    MINIMAL = auto()
    
    def __str__(self) -> str:
        if self == TRANSCRIPTION_TYPE.FULL:
            return "full"
        elif self == TRANSCRIPTION_TYPE.SIMPLE:
            return "simple"
        else:
            return "minimal"
    
    def from_str(name: str):
        if name == str(TRANSCRIPTION_TYPE.FULL):
            return TRANSCRIPTION_TYPE.FULL
        elif name == str(TRANSCRIPTION_TYPE.SIMPLE):
            return TRANSCRIPTION_TYPE.SIMPLE
        elif name == str(TRANSCRIPTION_TYPE.MINIMAL):
            return TRANSCRIPTION_TYPE.MINIMAL
        else:
            raise RuntimeError("Bad name")
        

CATEGORIES = (
    "dreams", "life",
    "funny", "pands/rus"
)

BASE_URL = "http://spokencorpora.ru"
_CATEGORY_URL = f"{BASE_URL}/showcorpus.py"
_FILE_URL = f"{BASE_URL}/showtrans.py"
        
        
def _get_stories_json(text: str) -> str:
    start_idx = text.find('{', text.find("datastr"))
    end_idx = start_idx
    curly_brackets_cnt = 0
    
    for idx, ch in enumerate(text[start_idx:]):
        if ch == '{': curly_brackets_cnt += 1
        elif ch == '}': curly_brackets_cnt -= 1
        
        if curly_brackets_cnt == 0:
            end_idx = idx + start_idx + 1
            break
    
    return text[start_idx:end_idx]
        
        
def _get_stories_dict(soup: BeautifulSoup) -> Union[str, None]:
    for tag in soup.find_all("script"):
        if tag.string and "datastr" in tag.string:
            try:
                json_data = _get_stories_json(tag.string).replace("\\\\", "\\")
                return json.loads(json_data)
            except Exception as e:
                print(e)
                break
    
    return None
        
        
def _get_stories(data: Union[str, bytes], parser: str) -> List[str]:
    soup = BeautifulSoup(data, parser)
    
    data = _get_stories_dict(soup)
    
    if isinstance(data, dict) and "rows" in data:
        rows = data["rows"]
        return [row["cell"][0] for row in rows if "_wr" not in row["cell"][0]]
        
    return []
        
        
def _get_stories_names(categories: List[str]) -> Dict[str, list]:
    category_to_stories = dict()
    params = {"dir": ""}
    
    for cat_idx, cat in enumerate(categories):
        params["dir"] = f"{cat_idx:02}{cat}"
        
        try:
            response = requests.get(
                url=_CATEGORY_URL,
                params=params
            )
        except requests.exceptions.RequestException as e:
            print(e)
            
        if response.ok:
            category_to_stories[params["dir"]] = _get_stories(
                data=response.content, 
                parser="lxml"
            )
        else:
            print(f"`{response.url}` cannot be loaded")
            
    return category_to_stories


def _get_story(story_dict: dict) -> Union[str, None]:
    if not isinstance(story_dict, dict):
        return None

    rows = story_dict["rows"]
    story = []
    
    for row in rows:
        # take only storyteller's text
        if len(row["cell"]) > 2:
            story.append(row["cell"][2])
    
    return '\n'.join(story)
        

def _download_story(file: str, transcription_type: TRANSCRIPTION_TYPE) -> Union[Tuple[str, str], None]:
    try:
        response = requests.get(
            url=_FILE_URL,
            params={
                "file": file,
                "type": int(transcription_type)        
            }
        )
    except requests.exceptions.RequestException as e:
        print(e)
        
    if response.ok:
        soup = BeautifulSoup(response.content, "lxml")
        
        story_dict = _get_stories_dict(soup)
        
        if story_dict is not None:
            return file, _get_story(story_dict)
    
    return None


def download_stories(
    transcription_type: TRANSCRIPTION_TYPE,
    dataset_dir: Union[Path, str]
):
    """Downloads stories from spokencorpora.ru

    Args:
        transcription_type (TRANSCRIPTION_TYPE): spokencorpora.ru has 3 types of transcription: full, simple, and minimal 
        dataset_dir (Union[Path, str]): directory name where to save after downloading
    """
    
    if not isinstance(dataset_dir, Path): 
        dataset_dir = Path(dataset_dir)
    
    if not dataset_dir.exists():
        dataset_dir.mkdir(parents=True)
        
    category_to_stories = _get_stories_names(CATEGORIES)
    files = [f"{cat}/{story_name}" for cat in category_to_stories.keys() for story_name in category_to_stories[cat]]
    
    with ThreadPoolExecutor(max_workers=len(files)) as executor:
        futures = [executor.submit(_download_story, file, transcription_type) for file in files]
        
        for idx, future in enumerate(as_completed(futures)):
            result = future.result()
            
            if result is not None:
                filename, story = result
                filename = dataset_dir / f"{filename.replace('/', '-')}.txt"
                save_txt(filename=filename, text=story)
        
        
parser = ArgumentParser()
parser.add_argument(
    "-d", "--dir",
    type=str,
    help="Directory to save dataset",
    default=Path(__file__).parent / "dataset"
)
parser.add_argument(
    "-t", "--transcription-type",
    type=str,
    choices=[
        str(TRANSCRIPTION_TYPE.FULL),
        str(TRANSCRIPTION_TYPE.SIMPLE),
        str(TRANSCRIPTION_TYPE.MINIMAL)
    ],
    help="Directory to save dataset",
    default=str(TRANSCRIPTION_TYPE.SIMPLE)
)

        
if __name__ == "__main__":
    
    args = parser.parse_args()
    
    dataset_dir = args.dir
    transcription_type = TRANSCRIPTION_TYPE.from_str(args.transcription_type)
    
    try:
        download_stories(
            transcription_type=transcription_type,
            dataset_dir=dataset_dir
        )
        print("Dataset downloaded successfully.")
    except Exception as e:
        print("Fail to download.")
        print(e)
    
    